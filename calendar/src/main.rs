use std::{vec, collections::HashSet};

use calendar::{Calendar, NaiveTimeInter};
use chrono::NaiveTime;
use clap::Parser;

#[derive(Parser)]
struct Arg{
    #[clap(short,  long)]
    fst:String,
    #[clap(short,  long)]
    snd:String,
    #[clap(short,  long)]
    duration:u64
}

const DEBUG: u8 = 0;

fn _strategy_one(cal1: &Calendar<NaiveTime>, cal2: &Calendar<NaiveTime>, dur : u64)->Vec<NaiveTimeInter>{
    // you should use the intersection of ferial day
    let bound_l = if cal1.get_bound_l() > cal2.get_bound_l() { *cal1.get_bound_l() } else { *cal2.get_bound_l() };
    let bound_h = if cal1.get_bound_r() < cal2.get_bound_r() { *cal1.get_bound_r() } else { *cal2.get_bound_r() };

    //TODO check if better to use an Result to handle possible error 
    let n_interval =  (bound_h - bound_l).num_minutes()/(dur as i64) ;
    let mut booking:Vec<NaiveTimeInter>  = Vec::with_capacity(n_interval.try_into().unwrap());
    let sch1_dim = cal1.get_schedule().len();    
    let sch2_dim = cal2.get_schedule().len();    

    // get the higher lower bound
    let mut start_t = bound_l;


    let (mut i, mut j) = (0, 0);
    for _k in 0..n_interval{

        if DEBUG==1{
            println!("start time{}", start_t);
        }

        let (mut exact, mut empty) = (false,false);
        let candidate = Calendar::convert_interval(start_t, dur as u32);

        if i<sch1_dim && Calendar::compatible_schedule(candidate,  cal1.get_schedule()[i], &mut exact){
            empty = true;
            if exact{
                i+=1;
            }
        } 
        
        if j<sch2_dim && Calendar::compatible_schedule(candidate,  cal2.get_schedule()[j], &mut exact){
            empty = true;
            if exact{
                j+=1;
            }
        }

        // TODO change to interval
        if empty{
            booking.push(candidate);
        }

        start_t = Calendar::time_from_duration(start_t, 30);

        // update interval
    }

    //FIXME
    booking

}
fn main() {

    let args= Arg::parse();
    let file1 = args.fst.as_str();
    let file2 = args.snd.as_str();
    let dur = args.duration;

if DEBUG==1{
    println!("cwd:{:?}", std::env::current_dir());
    println!("fst={}", file1);
    println!("snd={}", file2);
    println!("dur={:?}", dur);
}
    let cal1  = Calendar::from_file(file1).unwrap();
    let cal2  = Calendar::from_file(file2).unwrap();

    let mut start_time = if cal1.get_bound_l() > cal2.get_bound_l() { *cal1.get_bound_l() } else { *cal2.get_bound_l() };
    let end_time = if cal1.get_bound_r() < cal2.get_bound_r() { *cal1.get_bound_r() } else { *cal2.get_bound_r() };
  
    
    // use set and add from iterator
    let mut booking:HashSet<NaiveTimeInter> = HashSet::new();

    let mut iter1 =  cal1.get_schedule().into_iter();
    let mut iter2 =  cal2.get_schedule().into_iter();

    let  mut s1 = iter1.next();
    let mut s2 = iter2.next();


    while s1.is_some() || s2.is_some(){
        // FIXME INSERT INTERVAL ONLY WHEN NO 
        // find the first starting 
       
        if DEBUG==1{
            println!("s1\t{:?}", *s1.clone().unwrap());
            println!("s2\t{:?}", *s2.clone().unwrap());
        }


        // check if there is an intersection-> take the union
        if s1.is_some() && s2.is_some() && Calendar::schedule_intersection(*s1.clone().unwrap(), *s2.clone().unwrap()) {
            let s_u  =  Calendar::schedule_union(*s1.unwrap(), *s2.unwrap());
            
            // check if candidate compatible with both calender
            // if yes add it to the set
            if Calendar::interval_constraint(start_time, dur as i64, s_u){
                // modify to crete a larger interval
                let candidate = Calendar::create_interval(     start_time, s_u);

                println!("candidate\t{:?}",candidate);

                booking.insert(candidate); 
                start_time = Calendar::time_from_interval( s_u); 
               
            }
            
            s1 = iter1.next();
            s2 = iter2.next();

        }
        // NO INTERSECTION
        else{
            if  Calendar::interval_constraint(start_time, dur as i64,*s1.clone().unwrap()){
                let candidate = Calendar::create_interval(     start_time, *s1.unwrap());

                println!("candidate\t{:?}",candidate);

                booking.insert(candidate); 
    
                start_time = Calendar::time_from_interval( candidate); 
            }
            else{
                // change if end candidate slot bigger than start schedule slot
                
                s1 = iter1.next();
                start_time = Calendar::time_from_duration(start_time, dur as i64);   
            }

            if  Calendar::interval_constraint(start_time, dur as i64,*s2.clone().unwrap()){
                let candidate = Calendar::create_interval(     start_time, *s2.unwrap());

                println!("candidate\t{:?}",candidate);

                booking.insert(candidate); 
                start_time = Calendar::time_from_interval( candidate); 
            }
            else{
                s2 = iter2.next();
                start_time = Calendar::time_from_duration(start_time, dur as i64);   
            }
        }


    }

    // check if there some space after work
    let last_candidate = (start_time, end_time );

    if Calendar::duration_costraint(last_candidate, dur as i64){

        println!("last_candidate\t{:?}",last_candidate);

    }
        
}


    



#[cfg(test)]
mod test {

    use super::*;

    #[test]

    fn test_stategy_one(){
        let bounds1 =( NaiveTime::parse_from_str("08:00","%H:%M").unwrap() ,NaiveTime::parse_from_str("22:00","%H:%M").unwrap() ) ;
        
        let mut schedule1:Vec<(NaiveTime, NaiveTime)> = Vec::new();
        schedule1.push(( NaiveTime::parse_from_str("10:00","%H:%M").unwrap() ,NaiveTime::parse_from_str("11:30","%H:%M").unwrap() ));
        schedule1.push(( NaiveTime::parse_from_str("14:00","%H:%M").unwrap() ,NaiveTime::parse_from_str("16:00","%H:%M").unwrap() ));
    
        let cal1 = Calendar::new(schedule1.clone(), bounds1.clone());
        
        let bounds2 =( NaiveTime::parse_from_str("09:00","%H:%M").unwrap() ,NaiveTime::parse_from_str("18:00","%H:%M").unwrap() ) ;
        
        let mut schedule2:Vec<(NaiveTime, NaiveTime)> = Vec::new();
        schedule2.push(( NaiveTime::parse_from_str("09:30","%H:%M").unwrap() ,NaiveTime::parse_from_str("12:00","%H:%M").unwrap() ));
        schedule2.push(( NaiveTime::parse_from_str("13:30","%H:%M").unwrap() ,NaiveTime::parse_from_str("16:30","%H:%M").unwrap() ));
    
        let cal2 = Calendar::new(schedule2.clone(), bounds2.clone());
        let appointmt_check = vec![
            (NaiveTime::parse_from_str("09:00","%H:%M").unwrap(), NaiveTime::parse_from_str("09:30","%H:%M").unwrap()),
            (NaiveTime::parse_from_str("12:00","%H:%M").unwrap(), NaiveTime::parse_from_str("13:30","%H:%M").unwrap()),
            (NaiveTime::parse_from_str("16:30","%H:%M").unwrap(), NaiveTime::parse_from_str("18:00","%H:%M").unwrap()),
        ];

        let appointmt_res = strategy_one(&cal1, &cal2, 30);

        assert_eq!(appointmt_res, appointmt_check, "wrong list appointment ");


    }

}