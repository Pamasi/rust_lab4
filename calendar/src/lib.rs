use std::error::Error;
use std::io::{self, BufReader,prelude::*};
use std::fs::File;
use std::vec;
use chrono::{NaiveTime, Timelike, Duration};
use itertools::Itertools;
//use std::env;

pub struct Calendar<T> {
    schedule: Vec<(T, T)>,
    bounds: (T,T)
}

pub type  NaiveTimeInter = (NaiveTime, NaiveTime);

impl Calendar<NaiveTime>{

   

    fn parse_interval(s_inter: String ) -> NaiveTimeInter{
        s_inter.split(' ').map(|s| NaiveTime::parse_from_str(s,"%H:%M").unwrap())
                          .collect_tuple().expect("From file format")

    }    
    
    // fn parse_interval(s_inter: String ) -> NaiveTimeInter{
    //     s_inter.split(' ').map(|s| NaiveTime::parse_from_str(s,"%H:%M").unwrap())
    //                       .collect_tuple().expect("From file format")

    // }

    pub fn new(schedule:Vec<NaiveTimeInter>, bounds: NaiveTimeInter ) -> Self{
        Self{ schedule, bounds}
    } 

    pub fn get_bounds(& self ) -> & NaiveTimeInter{
        &self.bounds
    }    
     
    pub fn get_bound_l(& self ) -> & NaiveTime{
        &self.bounds.0
    }     
    pub fn get_bound_r(& self ) -> & NaiveTime{
        &self.bounds.1
    }     
    
    pub fn get_schedule(& self ) -> & Vec<NaiveTimeInter>{
        &self.schedule
    }    
  
    pub fn schedule_intersection(s1: NaiveTimeInter,  s2: NaiveTimeInter)->bool{
        if  s2.0<=s1.0 && s1.0<=s2.1 || s1.0<=s2.0 && s2.0<=s1.1{
            true
        }
        else{
            false
        }
    }

    pub fn schedule_union(s1: NaiveTimeInter,  s2: NaiveTimeInter)->NaiveTimeInter{
        if  s2.0<=s1.0 && s1.0<=s2.1{
            if s2.1<=s1.1{
                (s2.0, s1.1)
            }
            else{
                (s2.0, s2.1)
            }
        }
        else{
            if s2.1<=s1.1{
                (s1.0, s1.1)
            }
            else{
                (s1.0, s2.1)
            }
        }
        
    }
    pub fn time_from_duration(time: NaiveTime, duration : i64)->NaiveTime{
        time +Duration::minutes(duration)
    } 
    
    pub fn time_from_interval( interval : NaiveTimeInter)->NaiveTime{
        interval.1
    }


    pub fn compatible_schedule(candidate: NaiveTimeInter, schedule: NaiveTimeInter,exact: &mut bool)->bool{
        *exact = false;
        match candidate.1<=schedule.0{
            true => {   if candidate.1 == candidate.0 {
                            *exact =true;
                        }
                        true
                    },
            _=> false
        }
            
    }

    pub fn change_schedule(candidate: NaiveTimeInter, schedule: NaiveTimeInter)->bool{
        match candidate.1>=schedule.1{
            true =>  true,
            _=> false
        }
            
    }
    
    
    pub fn convert_interval(time: NaiveTime, duration : u32)-> NaiveTimeInter{
        (time, Self::time_from_duration(time, duration as i64))
    }
    
    pub fn create_interval(time: NaiveTime, costraint: NaiveTimeInter)-> NaiveTimeInter{
        (time, costraint.0)
    }
    pub fn interval_constraint(start: NaiveTime, constr_dur:i64, constr_inter: NaiveTimeInter)-> bool{
        if  (constr_inter.0- start ).num_minutes() >= constr_dur{
            true
        }
        else {
            false
        }
    }

    pub fn duration_costraint(candidate: NaiveTimeInter,constr_dur:i64)->bool{
        if  (candidate.1- candidate.0 ).num_minutes() >= constr_dur{
            true
        }
        else {
            false
        }
    }
    pub fn from_file(name: &str)-> Option<Calendar<NaiveTime>>{
        //println!("cwd:{:?}", std::env::current_dir());
        let file = File::open(name).expect("Missing file");
        
        let buf = BufReader::new(file);
        let mut schedule:Vec<(NaiveTime, NaiveTime)> = Vec::new();
        
        let mut iter =  buf.lines();

        let fst =iter.next().expect("Empty file!");

        let bounds= Self::parse_interval(fst.unwrap());



        for l in iter{
            if l.is_ok(){
                schedule.push(Self::parse_interval(l.unwrap()) );
            }
        }
    
     
        Some(Self{schedule, bounds})
         
    }

  
}


#[cfg(test)]

mod tests{

    use super::*;

    #[test]
    fn test_correct_creation(){
        let bounds =( NaiveTime::parse_from_str("08:00","%H:%M").unwrap() ,NaiveTime::parse_from_str("22:00","%H:%M").unwrap() ) ;
        let mut schedule:Vec<(NaiveTime, NaiveTime)> = Vec::new();

        schedule.push(( NaiveTime::parse_from_str("10:00","%H:%M").unwrap() ,NaiveTime::parse_from_str("11:30","%H:%M").unwrap() ));
        schedule.push(( NaiveTime::parse_from_str("13:00","%H:%M").unwrap() ,NaiveTime::parse_from_str("16:00","%H:%M").unwrap() ));
        let cal = Calendar::new(schedule.clone(), bounds.clone());
        
        assert_eq!(cal.schedule, schedule, "different scheduler");
        assert_eq!(cal.bounds, bounds, "different bounds");
    }    
    
    #[test]
    fn test_creation_from_file(){
        let cal_file = Calendar::from_file("file/cal1.txt");
        
        let bounds =( NaiveTime::parse_from_str("08:00","%H:%M").unwrap() ,NaiveTime::parse_from_str("20:00","%H:%M").unwrap() ) ;
        let mut schedule:Vec<(NaiveTime, NaiveTime)> = Vec::new();

        schedule.push(( NaiveTime::parse_from_str("10:00","%H:%M").unwrap() ,NaiveTime::parse_from_str("11:30","%H:%M").unwrap() ));
        schedule.push(( NaiveTime::parse_from_str("14:00","%H:%M").unwrap() ,NaiveTime::parse_from_str("16:00","%H:%M").unwrap() ));
        
        let cal_test = Calendar::new(schedule, bounds);

        if cal_file.is_some(){
            let  tmp = cal_file.unwrap();
            assert_eq!(cal_test.schedule, tmp.schedule, "different scheduler");
            assert_eq!(cal_test.bounds, tmp.bounds, "different bounds");
        }
        else{
            panic!("Impossible to read file");
        }


    }

    #[test]

    fn test_convert_interval(){
        let t1 =  NaiveTime::parse_from_str("08:00","%H:%M").unwrap() ;
        let t2 =  NaiveTime::parse_from_str("08:30","%H:%M").unwrap() ;
        let interval = Calendar::convert_interval(t1, 30);

        assert_eq!(interval, (t1, t2), "time interval not equal")

    }
}